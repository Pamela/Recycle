package com.example.pamela.recycle;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pamela on 19/08/2017.
 */

public class Regulation extends AppCompatActivity{
    private JSONArray arrayj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regulation);
        TextView tx = (TextView) findViewById(R.id.reg);


            try {
                arrayj = analisiJSON();
                String data="";
                for(int i=0; i<arrayj.length();i++) {
                    JSONObject jo = arrayj.getJSONObject(i);
                    String chiavi = jo.optString("CHIAVI").toString();
                    String rifiuto = jo.optString("RIFIUTO");
                    String dettagli = jo.optString("DETTAGLI");
                    String raccolta = jo.optString("RACCOLTA");
                    data=data+"\n \n Ciategoria: " + rifiuto + "\n Dettagli: " + dettagli + "\n Dove gettarlo:" + raccolta + "\n Altri oggetti in questa categoria: " + chiavi;
                }
                tx.setText(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        Button btn4 = (Button) findViewById(R.id.pre);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prepage= new Intent(Regulation.this,MainActivity.class);
                startActivity(prepage);
            }
        });

        }

    public JSONArray analisiJSON() throws JSONException {
        String in=loadJSONFromAsset();
        JSONObject reader = new JSONObject(in);
            JSONArray rifiuti = reader.getJSONArray("Rifiuti");
            return rifiuti;

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("garbage.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
