package com.example.pamela.recycle;

import android.content.Intent;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    public static JSONArray JA;
    public static JSONArray JA1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int err=1;
                try {
                    EditText etext= (EditText) findViewById(R.id.et);
                    TextView t1 = (TextView) findViewById(R.id.r2col1);
                    TextView t2 = (TextView) findViewById(R.id.r2col2);
                    TextView t3 = (TextView) findViewById(R.id.r2col3);
                    TextView t4 = (TextView) findViewById(R.id.r2col4);
                    TextView tl1 = (TextView) findViewById(R.id.r1col1);
                    TextView tl2 = (TextView) findViewById(R.id.r1col2);
                    TextView tl3 = (TextView) findViewById(R.id.r1col3);
                    TextView tl4 = (TextView) findViewById(R.id.r1col4);
                    JA = analisiJSON(0);
                    String Citta;
                    String umido;
                    String indifferenziata;
                    String ingombranti;
                    for(int i=0;i<JA.length();i++){
                        JSONObject jo = JA.getJSONObject(i);
                        Citta = jo.optString("Citta").toString();
                        if((i+1==JA.length())&&(err==1)){

                            t1.setText("Non ci sono informazioni sulla città scelta");
                            t2.setText("  ");
                            t3.setText("  ");
                            t4.setText("  ");
                            tl1.setText("  ");
                            tl2.setText("  ");
                            tl3.setText("  ");
                            tl4.setText("  ");
                        }
                        else if (Citta.equals(etext.getText().toString())) {
                            err=0;
                            tl1.setText("LUOGO            ");
                            t1.setText(Citta);
                            umido = jo.optString("Raccolta_umido");
                            tl2.setText("RACCOLTA UMIDO     ");
                            t2.setText(umido);
                            indifferenziata = jo.optString("Raccolta_indifferenziata");
                            tl3.setText("RACCOLTA INDIFFERENZIATA     ");
                            t3.setText(indifferenziata );
                            ingombranti = jo.optString("Raccolta_ingombranti");
                            tl4.setText("RACCOLTA INGOMBRANTI");
                            t4.setText(ingombranti );
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Button btn2 = (Button) findViewById(R.id.button);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView textinfo = (TextView) findViewById(R.id.info);
                try {
                    JA1 = analisiJSON(1);
                    String rifiuto;
                    String dettagli;
                    String raccolta;
                    String chiavi;
                    String data="";
                    EditText e_text1 = (EditText) findViewById(R.id.et1);
                    data=e_text1.getText().toString();
                    for(int i=0; i<JA1.length();i++){
                        JSONObject jo = JA1.getJSONObject(i);
                        chiavi = jo.optString("CHIAVI").toString();
                        if(chiavi.contains(data)){
                            rifiuto= jo.optString("RIFIUTO");
                            dettagli= jo.optString("DETTAGLI");
                            raccolta= jo.optString("RACCOLTA");
                            textinfo.setText("Ciategoria: "+ rifiuto +"\n Dettagli: "+ dettagli +"\n Dove gettarlo:"+ raccolta +"\n Altri oggetti in questa categoria: "+ chiavi);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Button btn3 =(Button) findViewById(R.id.button3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regulationpage= new Intent(MainActivity.this,Regulation.class);
                startActivity(regulationpage);

            }
        });

    }

    public JSONArray analisiJSON(int i) throws JSONException {
        String in=loadJSONFromAsset();
        JSONObject reader = new JSONObject(in);
        if(i==0){
            JSONArray orari = reader.getJSONArray("ORARIO");
            return orari;}
        else if(i==1){
            JSONArray rifiuti = reader.getJSONArray("Rifiuti");
            return rifiuti;
        }
        return null;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("garbage.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
       return json;

    }





}
